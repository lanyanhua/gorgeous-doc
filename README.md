# gorgeous-doc

#### 介绍

通过git拉取项目文件进行解析注释注解生成API文档  
零侵入，无需任何配置，独立运行，注释生成文档，支持读取swagger注解，自定义注释配置，在线调试  

无损替换： 
项目已引用swagger注解包，可以读取swagger注释  
支持自定义注释读取，使用特定注释读取项目也可以无损替换  

支持微服务多模块项目

#### 开始使用
文档已更新 [传送门](https://gitee.com/lanyanhua/gorgeous-doc/wikis/pages)   
后续有时间录一个视频

#### 演示地址

[传送门](http://101.35.125.172:5160/gorgeous)

#### 联系方式

qq: 3092575337 (推荐)  
wx: lanyanhua1024  
邮箱: lanyanhua1024@163.com  
