# use `gorgeous-doc`;

# 删除所有表，重新启动项目，初始化表结构
drop table api_info;
drop table api_param;
drop table class_field;
drop table class_info;
drop table env_info;
drop table menu;
drop table notes_config;
drop table project;
drop table project_branch;
drop table project_config;