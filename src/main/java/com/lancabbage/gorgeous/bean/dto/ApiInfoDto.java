package com.lancabbage.gorgeous.bean.dto;

import com.lancabbage.gorgeous.bean.po.ApiInfo;

import java.util.List;

/**
 * @ClassName: ApiInfoDto
 * @Description:TODO ()
 * @author: lanyanhua
 * @date: 2020/12/3 8:25 上午
 * @Copyright:
 */
public class ApiInfoDto extends ApiInfo {

    /**
     * 目标API ID
     */
    private Integer targetId;

    private List<ApiParamDto> apiParamList;

    /**
     * 应用ID
     */
    private Integer applicationId;

    public Integer getApplicationId() {
        return applicationId;
    }

    public void setApplicationId(Integer applicationId) {
        this.applicationId = applicationId;
    }

    public Integer getTargetId() {
        return targetId;
    }

    public void setTargetId(Integer targetId) {
        this.targetId = targetId;
    }

    public List<ApiParamDto> getApiParamList() {
        return apiParamList;
    }

    public void setApiParamList(List<ApiParamDto> apiParamList) {
        this.apiParamList = apiParamList;
    }


}
