package com.lancabbage.gorgeous.bean.dto;

import java.util.List;

/**
 * @author: lanyanhua
 * @date: 2021/7/29 7:25 下午
 * @Description:
 */
public class MenuCriteria {

    /**
     * 项目ID
     */
    private List<Integer> projectIdList;
    /**
     * 分支ID
     */
    private List<Integer> branchIdList;

    /**
     * 父菜单ID
     */
    private List<Integer> parentIdList;

    public List<Integer> getProjectIdList() {
        return projectIdList;
    }

    public void setProjectIdList(List<Integer> projectIdList) {
        this.projectIdList = projectIdList;
    }

    public List<Integer> getBranchIdList() {
        return branchIdList;
    }

    public void setBranchIdList(List<Integer> branchIdList) {
        this.branchIdList = branchIdList;
    }

    public List<Integer> getParentIdList() {
        return parentIdList;
    }

    public void setParentIdList(List<Integer> parentIdList) {
        this.parentIdList = parentIdList;
    }
}
