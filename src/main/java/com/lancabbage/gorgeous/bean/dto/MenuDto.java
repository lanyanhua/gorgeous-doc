package com.lancabbage.gorgeous.bean.dto;

import com.lancabbage.gorgeous.bean.po.Menu;

import java.util.List;

/**
 * @author: lanyanhua
 * @date: 2020/12/5 6:45 下午
 * @Description:
 */
public class MenuDto extends Menu {

    /**
     * 项目配置名称
     */
    private String pcName;

    /**
     * 接口名称
     */
    private String apiName;

    /**
     * 接口类型：0:all 1:post 2:get 3:delete 4:put
     */
    private Integer type;

    /**
     * 访问路径
     */
    private String path;

    /**
     * 是否删除
     */
    private Boolean delete;

    private List<MenuDto> childrenMenu;
    private List<ApiInfoDto> apiInfos;

    public String getPcName() {
        return pcName;
    }

    public void setPcName(String pcName) {
        this.pcName = pcName;
    }

    public String getApiName() {
        return apiName;
    }

    public void setApiName(String apiName) {
        this.apiName = apiName;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public Boolean getDelete() {
        return delete;
    }

    public void setDelete(Boolean delete) {
        this.delete = delete;
    }

    public List<MenuDto> getChildrenMenu() {
        return childrenMenu;
    }

    public void setChildrenMenu(List<MenuDto> childrenMenu) {
        this.childrenMenu = childrenMenu;
    }

    public List<ApiInfoDto> getApiInfos() {
        return apiInfos;
    }

    public void setApiInfos(List<ApiInfoDto> apiInfos) {
        this.apiInfos = apiInfos;
    }
}
