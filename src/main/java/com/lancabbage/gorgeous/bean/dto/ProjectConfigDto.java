package com.lancabbage.gorgeous.bean.dto;


import com.lancabbage.gorgeous.bean.po.ProjectConfig;

/**
 * @author: lanyanhua
 * @date: 2021/8/30 7:56 下午
 * @Description:
 */
public class ProjectConfigDto extends ProjectConfig {


    /**
     * 应用ID
     */
    private Integer applicationId;

    public Integer getApplicationId() {
        return applicationId;
    }

    public void setApplicationId(Integer applicationId) {
        this.applicationId = applicationId;
    }
}
