package com.lancabbage.gorgeous.bean.dto;

import com.lancabbage.gorgeous.bean.po.Project;

import java.util.List;

/**
 * @author: lanyanhua
 * @date: 2020/12/5 6:45 下午
 * @Description:
 */
public class ProjectDto extends Project {

    /**
     * 分支
     */
    private List<ProjectBranchDto> branchList;
    /**
     * 菜单
     */
    private List<MenuDto> menuList;
    /**
     * 项目配置
     */
    private List<ProjectConfigDto> projectConfigs;

    public List<ProjectBranchDto> getBranchList() {
        return branchList;
    }

    public void setBranchList(List<ProjectBranchDto> branchList) {
        this.branchList = branchList;
    }

    public List<MenuDto> getMenuList() {
        return menuList;
    }

    public void setMenuList(List<MenuDto> menuList) {
        this.menuList = menuList;
    }

    public List<ProjectConfigDto> getProjectConfigs() {
        return projectConfigs;
    }

    public void setProjectConfigs(List<ProjectConfigDto> projectConfigs) {
        this.projectConfigs = projectConfigs;
    }
}
