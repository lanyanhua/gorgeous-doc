package com.lancabbage.gorgeous.bean.dto;

import java.io.Serializable;

/**
 * @author lanyanhua
 * @date 2021/10/20 5:10 下午
 * @Description
 */
public class WebHookReqDto implements Serializable {

    /**
     * after : 37cc202d3a5f41b868ca2108c2ed392938a372ac
     * before : 33a5782f3dd4f60e3566d344f0d6a191ae46bdc5
     * commits : [{"added":[],"author":{"email":"lanyanhua1024@163.com","id":1793098,"name":"lancabbage","remark":null,"time":"2021-10-20T14:33:42+08:00","url":"https://gitee.com/lanyanhua","user":null,"user_name":"lanyanhua","username":"lanyanhua"},"committer":{"email":"lanyanhua1024@163.com","id":1793098,"name":"lancabbage","remark":null,"time":null,"url":"https://gitee.com/lanyanhua","user":null,"user_name":"lanyanhua","username":"lanyanhua"},"distinct":true,"id":"37cc202d3a5f41b868ca2108c2ed392938a372ac","message":"测试回调\n","modified":["src/main/java/com/lancabbage/gorgeous/controller/WebHookController.java"],"parent_ids":["33a5782f3dd4f60e3566d344f0d6a191ae46bdc5"],"removed":[],"timestamp":"2021-10-20T14:33:42+08:00","tree_id":"47e8ae3410e2052ff5d6673622a581d5e35ffa58","url":"https://gitee.com/lanyanhua/gorgeous-doc/commit/37cc202d3a5f41b868ca2108c2ed392938a372ac"}]
     * commits_more_than_ten : false
     * compare : https://gitee.com/lanyanhua/gorgeous-doc/compare/33a5782f3dd4f60e3566d344f0d6a191ae46bdc5...37cc202d3a5f41b868ca2108c2ed392938a372ac
     * created : false
     * deleted : false
     * enterprise : null
     * head_commit : {"added":[],"author":{"email":"lanyanhua1024@163.com","id":1793098,"name":"lancabbage","remark":null,"time":"2021-10-20T14:33:42+08:00","url":"https://gitee.com/lanyanhua","user":null,"user_name":"lanyanhua","username":"lanyanhua"},"committer":{"email":"lanyanhua1024@163.com","id":1793098,"name":"lancabbage","remark":null,"time":null,"url":"https://gitee.com/lanyanhua","user":null,"user_name":"lanyanhua","username":"lanyanhua"},"distinct":true,"id":"37cc202d3a5f41b868ca2108c2ed392938a372ac","message":"测试回调\n","modified":["src/main/java/com/lancabbage/gorgeous/controller/WebHookController.java"],"parent_ids":["33a5782f3dd4f60e3566d344f0d6a191ae46bdc5"],"removed":[],"timestamp":"2021-10-20T14:33:42+08:00","tree_id":"47e8ae3410e2052ff5d6673622a581d5e35ffa58","url":"https://gitee.com/lanyanhua/gorgeous-doc/commit/37cc202d3a5f41b868ca2108c2ed392938a372ac"}
     * hook_id : 791339
     * hook_name : push_hooks
     * hook_url : https://gitee.com/lanyanhua/gorgeous-doc/hooks/791339/edit
     * password :
     * project : {"clone_url":"https://gitee.com/lanyanhua/gorgeous-doc.git","created_at":"2020-12-01T15:48:42+08:00","default_branch":"release-script","description":"通过git拉去代码，解析源码生成项目API文档，不需要任何配置就能为项目生成API文档。零侵入式","fork":false,"forks_count":4,"full_name":"lanyanhua/gorgeous-doc","git_http_url":"https://gitee.com/lanyanhua/gorgeous-doc.git","git_ssh_url":"git@gitee.com:lanyanhua/gorgeous-doc.git","git_svn_url":"svn://gitee.com/lanyanhua/gorgeous-doc","git_url":"git://gitee.com/lanyanhua/gorgeous-doc.git","has_issues":true,"has_pages":false,"has_wiki":true,"homepage":"","html_url":"https://gitee.com/lanyanhua/gorgeous-doc","id":13007940,"language":"Java","license":"Apache-2.0","name":"gorgeous-doc","name_with_namespace":"lancabbage/gorgeous-doc","namespace":"lanyanhua","open_issues_count":2,"owner":{"avatar_url":"https://gitee.com/assets/no_portrait.png","email":"lanyanhua1024@163.com","html_url":"https://gitee.com/lanyanhua","id":1793098,"login":"lanyanhua","name":"lancabbage","remark":null,"site_admin":false,"type":"User","url":"https://gitee.com/lanyanhua","user_name":"lanyanhua","username":"lanyanhua"},"path":"gorgeous-doc","path_with_namespace":"lanyanhua/gorgeous-doc","private":false,"pushed_at":"2021-10-20T14:34:20+08:00","ssh_url":"git@gitee.com:lanyanhua/gorgeous-doc.git","stargazers_count":24,"svn_url":"svn://gitee.com/lanyanhua/gorgeous-doc","updated_at":"2021-10-20T14:34:21+08:00","url":"https://gitee.com/lanyanhua/gorgeous-doc","watchers_count":10}
     * push_data : null
     * pusher : {"email":"lanyanhua1024@163.com","id":1793098,"name":"lancabbage","url":"https://gitee.com/lanyanhua","user_name":"lanyanhua","username":"lanyanhua"}
     * ref : refs/heads/webHook
     * repository : {"clone_url":"https://gitee.com/lanyanhua/gorgeous-doc.git","created_at":"2020-12-01T15:48:42+08:00","default_branch":"release-script","description":"通过git拉去代码，解析源码生成项目API文档，不需要任何配置就能为项目生成API文档。零侵入式","fork":false,"forks_count":4,"full_name":"lanyanhua/gorgeous-doc","git_http_url":"https://gitee.com/lanyanhua/gorgeous-doc.git","git_ssh_url":"git@gitee.com:lanyanhua/gorgeous-doc.git","git_svn_url":"svn://gitee.com/lanyanhua/gorgeous-doc","git_url":"git://gitee.com/lanyanhua/gorgeous-doc.git","has_issues":true,"has_pages":false,"has_wiki":true,"homepage":"","html_url":"https://gitee.com/lanyanhua/gorgeous-doc","id":13007940,"language":"Java","license":"Apache-2.0","name":"gorgeous-doc","name_with_namespace":"lancabbage/gorgeous-doc","namespace":"lanyanhua","open_issues_count":2,"owner":{"avatar_url":"https://gitee.com/assets/no_portrait.png","email":"lanyanhua1024@163.com","html_url":"https://gitee.com/lanyanhua","id":1793098,"login":"lanyanhua","name":"lancabbage","remark":null,"site_admin":false,"type":"User","url":"https://gitee.com/lanyanhua","user_name":"lanyanhua","username":"lanyanhua"},"path":"gorgeous-doc","path_with_namespace":"lanyanhua/gorgeous-doc","private":false,"pushed_at":"2021-10-20T14:34:20+08:00","ssh_url":"git@gitee.com:lanyanhua/gorgeous-doc.git","stargazers_count":24,"svn_url":"svn://gitee.com/lanyanhua/gorgeous-doc","updated_at":"2021-10-20T14:34:21+08:00","url":"https://gitee.com/lanyanhua/gorgeous-doc","watchers_count":10}
     * sender : {"avatar_url":"https://gitee.com/assets/no_portrait.png","email":"lanyanhua1024@163.com","html_url":"https://gitee.com/lanyanhua","id":1793098,"login":"lanyanhua","name":"lancabbage","remark":null,"site_admin":false,"type":"User","url":"https://gitee.com/lanyanhua","user_name":"lanyanhua","username":"lanyanhua"}
     * sign : 12dsEfFCMIQHXxPDRQD/sfDk2mvsIsnv6rHhG+nauuQ=
     * timestamp : 1634711632544
     * total_commits_count : 1
     * user : {"email":"lanyanhua1024@163.com","id":1793098,"name":"lancabbage","url":"https://gitee.com/lanyanhua","user_name":"lanyanhua","username":"lanyanhua"}
     * user_id : 1793098
     * user_name : lancabbage
     */

    private String ref;
    private RepositoryBean repository;
    private String sign;
    private String timestamp;

    public static class RepositoryBean implements Serializable {
        /**
         * clone_url : https://gitee.com/lanyanhua/gorgeous-doc.git
         * created_at : 2020-12-01T15:48:42+08:00
         * default_branch : release-script
         * description : 通过git拉去代码，解析源码生成项目API文档，不需要任何配置就能为项目生成API文档。零侵入式
         * fork : false
         * forks_count : 4
         * full_name : lanyanhua/gorgeous-doc
         * git_http_url : https://gitee.com/lanyanhua/gorgeous-doc.git
         * git_ssh_url : git@gitee.com:lanyanhua/gorgeous-doc.git
         * git_svn_url : svn://gitee.com/lanyanhua/gorgeous-doc
         * git_url : git://gitee.com/lanyanhua/gorgeous-doc.git
         * has_issues : true
         * has_pages : false
         * has_wiki : true
         * homepage :
         * html_url : https://gitee.com/lanyanhua/gorgeous-doc
         * id : 13007940
         * language : Java
         * license : Apache-2.0
         * name : gorgeous-doc
         * name_with_namespace : lancabbage/gorgeous-doc
         * namespace : lanyanhua
         * open_issues_count : 2
         * owner : {"avatar_url":"https://gitee.com/assets/no_portrait.png","email":"lanyanhua1024@163.com","html_url":"https://gitee.com/lanyanhua","id":1793098,"login":"lanyanhua","name":"lancabbage","remark":null,"site_admin":false,"type":"User","url":"https://gitee.com/lanyanhua","user_name":"lanyanhua","username":"lanyanhua"}
         * path : gorgeous-doc
         * path_with_namespace : lanyanhua/gorgeous-doc
         * private : false
         * pushed_at : 2021-10-20T14:34:20+08:00
         * ssh_url : git@gitee.com:lanyanhua/gorgeous-doc.git
         * stargazers_count : 24
         * svn_url : svn://gitee.com/lanyanhua/gorgeous-doc
         * updated_at : 2021-10-20T14:34:21+08:00
         * url : https://gitee.com/lanyanhua/gorgeous-doc
         * watchers_count : 10
         */

        private String clone_url;
        private String git_http_url;
        private String git_ssh_url;
        private String git_svn_url;
        private String git_url;
        private String name;
        private String name_with_namespace;
        private String namespace;
        private int open_issues_count;
        private String ssh_url;
        private String svn_url;
        private String url;

        public String getClone_url() {
            return clone_url;
        }

        public void setClone_url(String clone_url) {
            this.clone_url = clone_url;
        }

        public String getGit_http_url() {
            return git_http_url;
        }

        public void setGit_http_url(String git_http_url) {
            this.git_http_url = git_http_url;
        }

        public String getGit_ssh_url() {
            return git_ssh_url;
        }

        public void setGit_ssh_url(String git_ssh_url) {
            this.git_ssh_url = git_ssh_url;
        }

        public String getGit_svn_url() {
            return git_svn_url;
        }

        public void setGit_svn_url(String git_svn_url) {
            this.git_svn_url = git_svn_url;
        }

        public String getGit_url() {
            return git_url;
        }

        public void setGit_url(String git_url) {
            this.git_url = git_url;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getName_with_namespace() {
            return name_with_namespace;
        }

        public void setName_with_namespace(String name_with_namespace) {
            this.name_with_namespace = name_with_namespace;
        }

        public String getNamespace() {
            return namespace;
        }

        public void setNamespace(String namespace) {
            this.namespace = namespace;
        }

        public int getOpen_issues_count() {
            return open_issues_count;
        }

        public void setOpen_issues_count(int open_issues_count) {
            this.open_issues_count = open_issues_count;
        }

        public String getSsh_url() {
            return ssh_url;
        }

        public void setSsh_url(String ssh_url) {
            this.ssh_url = ssh_url;
        }

        public String getSvn_url() {
            return svn_url;
        }

        public void setSvn_url(String svn_url) {
            this.svn_url = svn_url;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }
    }

    public String getRef() {
        return ref;
    }

    public void setRef(String ref) {
        this.ref = ref;
    }

    public RepositoryBean getRepository() {
        return repository;
    }

    public void setRepository(RepositoryBean repository) {
        this.repository = repository;
    }

    public String getSign() {
        return sign;
    }

    public void setSign(String sign) {
        this.sign = sign;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }
}
