package com.lancabbage.gorgeous.bean.vo.menu;

import java.util.List;

/**
 * @author: lanyanhua
 * @date: 2020/12/6 6:31 下午
 * @Description:
 */
public class MenuApiVo {

    private Integer id;

    /**
     * 菜单名称
     */
    private String menuName;

    /**
     * 项目配置名称
     */
    private String pcName;

    /**
     * controller
     */
    private String className;

    /**
     * 当前菜单对应API ID
     */
    private Integer apiId;
    /**
     * 接口名称
     */
    private String apiName;

    /**
     * 接口类型：0:all 1:post 2:get 3:delete 4:put
     */
    private Integer type;

    /**
     * 访问路径
     */
    private String path;

    /**
     * 项目ID
     */
    private Integer projectId;

    /**
     * 应用ID
     */
    private Integer applicationId;


    /**
     * 子菜单
     */
    private List<MenuApiVo> childrenMenu;

    public String getPcName() {
        return pcName;
    }

    public void setPcName(String pcName) {
        this.pcName = pcName;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getMenuName() {
        return menuName;
    }

    public void setMenuName(String menuName) {
        this.menuName = menuName;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public Integer getApiId() {
        return apiId;
    }

    public void setApiId(Integer apiId) {
        this.apiId = apiId;
    }

    public List<MenuApiVo> getChildrenMenu() {
        return childrenMenu;
    }

    public void setChildrenMenu(List<MenuApiVo> childrenMenu) {
        this.childrenMenu = childrenMenu;
    }

    public String getApiName() {
        return apiName;
    }

    public void setApiName(String apiName) {
        this.apiName = apiName;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public Integer getApplicationId() {
        return applicationId;
    }

    public void setApplicationId(Integer applicationId) {
        this.applicationId = applicationId;
    }

    public Integer getProjectId() {
        return projectId;
    }

    public void setProjectId(Integer projectId) {
        this.projectId = projectId;
    }
}
