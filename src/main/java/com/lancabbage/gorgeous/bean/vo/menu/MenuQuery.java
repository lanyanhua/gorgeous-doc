package com.lancabbage.gorgeous.bean.vo.menu;

import java.util.Objects;

/**
 * @author: lanyanhua
 * @date: 2021/7/31 4:39 下午
 * @Description:
 */
public class MenuQuery {

    /**
     * 项目ID
     */
    private Integer projectId;

    /**
     * 分支ID
     */
    private Integer branchId;

    /**
     * 是否super项目
     */
    private Integer isSuper;

    public Integer getProjectId() {
        return projectId;
    }

    public void setProjectId(Integer projectId) {
        this.projectId = projectId;
    }

    public Integer getBranchId() {
        return branchId;
    }

    public void setBranchId(Integer branchId) {
        this.branchId = branchId;
    }

    public Integer getIsSuper() {
        return isSuper;
    }

    public void setIsSuper(Integer isSuper) {
        this.isSuper = isSuper;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MenuQuery menuQuery = (MenuQuery) o;
        return Objects.equals(getProjectId(), menuQuery.getProjectId()) && Objects.equals(getBranchId(), menuQuery.getBranchId()) && Objects.equals(getIsSuper(), menuQuery.getIsSuper());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getProjectId(), getBranchId(), getIsSuper());
    }
}
