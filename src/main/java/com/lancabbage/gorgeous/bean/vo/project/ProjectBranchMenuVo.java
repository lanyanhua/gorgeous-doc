package com.lancabbage.gorgeous.bean.vo.project;

import com.lancabbage.gorgeous.bean.vo.menu.MenuApiVo;

import java.util.List;

/**
 * @author: lanyanhua
 * @date: 2021/7/29 5:57 下午
 * @Description:
 */
public class ProjectBranchMenuVo {
    /**
     * ID
     */
    private Integer id;

    /**
     * 分支名称
     */
    private String name;

    /**
     * 菜单列表
     */
    private List<MenuApiVo> menuList;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<MenuApiVo> getMenuList() {
        return menuList;
    }

    public void setMenuList(List<MenuApiVo> menuList) {
        this.menuList = menuList;
    }
}
