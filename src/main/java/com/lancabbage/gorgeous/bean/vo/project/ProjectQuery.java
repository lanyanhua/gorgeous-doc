package com.lancabbage.gorgeous.bean.vo.project;

/**
 * @author: lanyanhua
 * @date: 2021/7/31 4:39 下午
 * @Description:
 */
public class ProjectQuery {

    /**
     * 是否展示
     */
    private Boolean isShow;

    public Boolean getIsShow() {
        return isShow;
    }

    public void setIsShow(Boolean show) {
        isShow = show;
    }
}
