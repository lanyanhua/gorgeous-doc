package com.lancabbage.gorgeous.bean.vo.project;

import java.util.List;

/**
 * @author: lanyanhua
 * @date: 2020/12/5 3:41 下午
 * @Description:
 */
public class ProjectTreeVo {
    /**
     * ID
     */
    private Integer id;

    /**
     * 项目名称
     */
    private String name;
    /**
     * 分支
     */
    private List<ProjectBranchMenuVo> branchList;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<ProjectBranchMenuVo> getBranchList() {
        return branchList;
    }

    public void setBranchList(List<ProjectBranchMenuVo> branchList) {
        this.branchList = branchList;
    }
}
