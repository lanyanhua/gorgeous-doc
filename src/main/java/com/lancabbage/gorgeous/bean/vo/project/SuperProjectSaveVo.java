package com.lancabbage.gorgeous.bean.vo.project;


import com.lancabbage.gorgeous.bean.vo.menu.MenuVo;

import java.util.List;

/**
 * @author: lanyanhua
 * @date: 2020/12/5 5:26 下午
 * @Description:
 */
public class SuperProjectSaveVo {

    private Integer id;

    /**
     * 项目名称
     */
    private String name;

    /**
     * 是否展示
     */
    private Boolean isShow;

    /**
     * 展示顺序
     */
    private Integer displayOrder;

    /**
     * 菜单列表
     */
    private List<MenuVo> menuList;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Boolean getIsShow() {
        return isShow;
    }

    public void setIsShow(Boolean isShow) {
        this.isShow = isShow;
    }

    public Integer getDisplayOrder() {
        return displayOrder;
    }

    public void setDisplayOrder(Integer displayOrder) {
        this.displayOrder = displayOrder;
    }

    public List<MenuVo> getMenuList() {
        return menuList;
    }

    public void setMenuList(List<MenuVo> menuList) {
        this.menuList = menuList;
    }
}
