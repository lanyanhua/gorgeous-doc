package com.lancabbage.gorgeous.controller;

import com.alibaba.fastjson.JSON;
import com.lancabbage.gorgeous.bean.dto.ProjectBranchDto;
import com.lancabbage.gorgeous.bean.dto.WebHookReqDto;
import com.lancabbage.gorgeous.config.GitInfoConfig;
import com.lancabbage.gorgeous.service.ProjectBranchService;
import org.apache.tomcat.util.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;

/**
 * @author lanyanhua
 * @date 2021/10/11 5:13 下午
 * @Description
 */
@RestController
public class WebHookController {
    private final Logger log = LoggerFactory.getLogger(this.getClass());
    private final ProjectBranchService branchService;
    private final GitInfoConfig gitInfoConfig;

    public WebHookController(ProjectBranchService branchService, GitInfoConfig gitInfoConfig) {
        this.branchService = branchService;
        this.gitInfoConfig = gitInfoConfig;
    }

    @PostMapping("/webhook/send")
    public String webHook(@RequestBody WebHookReqDto data) {
        log.info(JSON.toJSONString(data));
        //签名验证
        try {
            String secret = gitInfoConfig.getWebHookSecret();
            if(StringUtils.hasText(secret)){
                return "webhook签名密钥未配置";
            }
            String timestamp = data.getTimestamp();
            String stringToSign = timestamp + "\n" + secret;
            Mac mac = Mac.getInstance("HmacSHA256");
            mac.init(new SecretKeySpec(secret.getBytes(StandardCharsets.UTF_8), "HmacSHA256"));
            byte[] signData = mac.doFinal(stringToSign.getBytes(StandardCharsets.UTF_8));
            String encode = new String(Base64.encodeBase64(signData));
            if (!encode.equals(data.getSign())) {
                return "签名验证失败";
            }
        } catch (Exception e) {
            log.error("签名验证异常", e);
            return "签名验证异常" + e.getMessage();
        }
        //获取项目分支信息
        WebHookReqDto.RepositoryBean r = data.getRepository();
        ProjectBranchDto branchDto = branchService.getProjectByUrlAndBranchName(Arrays.asList(r.getClone_url(), r.getGit_http_url()
                , r.getGit_ssh_url(), r.getGit_url(), r.getGit_http_url()), data.getRef());
        if (branchDto == null) {
            return "当前项目分支不存在";
        }
        branchService.pullProjectBranch(branchDto);

        return "200";
    }

    public static void main(String[] args) throws Exception {
//        String secret = UUID.randomUUID().toString(); 2
        String secret = "09edec7cc87f41f8996a2b08d0ee0419";
//        Long timestamp = System.currentTimeMillis();
        Long timestamp = 1633946778415L;
        String stringToSign = timestamp + "\n" + secret;
        System.out.println(stringToSign);
        Mac mac = Mac.getInstance("HmacSHA256");
        mac.init(new SecretKeySpec(secret.getBytes("UTF-8"), "HmacSHA256"));
        byte[] signData = mac.doFinal(stringToSign.getBytes("UTF-8"));
//        String encode = URLEncoder.encode(new String(Base64.encodeBase64(signData)), "UTF-8");
        String encode = new String(Base64.encodeBase64(signData));
        System.out.println(encode);
        //rlRBcDCCrnaL7gMFo2tKfIsoilDoMR+yGGmUcJPRNQ8=
        System.out.println(URLEncoder.encode("rlRBcDCCrnaL7gMFo2tKfIsoilDoMR+yGGmUcJPRNQ8=", "UTF-8"));
        //rlRBcDCCrnaL7gMFo2tKfIsoilDoMR%2ByGGmUcJPRNQ8%3D
    }
}
