package com.lancabbage.gorgeous.controller.menu;

import com.lancabbage.gorgeous.bean.dto.ApiInfoDto;
import com.lancabbage.gorgeous.bean.dto.ClassInfoDto;
import com.lancabbage.gorgeous.bean.dto.MenuDto;
import com.lancabbage.gorgeous.bean.vo.api.ApiInfoVo;
import com.lancabbage.gorgeous.bean.vo.base.BaseResponse;
import com.lancabbage.gorgeous.bean.vo.classInfo.ClassInfoVo;
import com.lancabbage.gorgeous.bean.vo.menu.BranchMenuVo;
import com.lancabbage.gorgeous.bean.vo.menu.MenuQuery;
import com.lancabbage.gorgeous.bean.vo.menu.MenuVo;
import com.lancabbage.gorgeous.map.MenuDtoToVo;
import com.lancabbage.gorgeous.service.ApiInfoService;
import com.lancabbage.gorgeous.service.ClassInfoService;
import com.lancabbage.gorgeous.service.MenuService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

/**
 * 菜单管理
 *
 * @author: lanyanhua
 * @date: 2020/12/6 6:30 下午
 * @Description:
 */
@RestController
@RequestMapping("/menu")
public class MenuController {

    private final MenuService menuService;
    private final ApiInfoService apiInfoService;
    private final ClassInfoService classInfoService;
    private final MenuDtoToVo menuDtoToVo;

    public MenuController(MenuService menuService, ApiInfoService apiInfoService, ClassInfoService classInfoService, MenuDtoToVo menuDtoToVo) {
        this.menuService = menuService;
        this.apiInfoService = apiInfoService;
        this.classInfoService = classInfoService;
        this.menuDtoToVo = menuDtoToVo;
    }

    /**
     * 查询菜单
     *
     * @param query 项目ID 分支ID
     * @return 菜单列表 API数据
     */
    @GetMapping("/listMenuByBranchId")
    public BaseResponse<BranchMenuVo> listMenuByQuery(MenuQuery query) {
        BranchMenuVo branchMenuVo = new BranchMenuVo();
        //菜单
        List<MenuDto> menuList = menuService.listMenuByBranchId(query.getBranchId());
        List<MenuVo> menuVos = menuDtoToVo.listMenuDtoToVo(menuList);
        branchMenuVo.setMenuList(menuVos);
        //API
        List<ApiInfoDto> apiInfoDtoList = apiInfoService.listApiByQuery(query);
        List<ApiInfoVo> apiInfoVos = menuDtoToVo.listApiInfoDtoToVo(apiInfoDtoList);
        branchMenuVo.setApiInfoList(apiInfoVos);
        //class
        List<ClassInfoDto> dtoList;
        if (query.getIsSuper() == 1) {
            //super项目查询 关联项目所有class
            List<Integer> queries = apiInfoDtoList.stream()
                    .map(ApiInfoDto::getId)
                    .distinct()
                    .collect(Collectors.toList());
            dtoList = classInfoService.listClassByApiIds(queries);
        } else {
            dtoList = classInfoService.listClassByBranchId(query.getBranchId());
        }
        List<ClassInfoVo> classInfoVos = menuDtoToVo.listClassInfoDtoToVo(dtoList);
        branchMenuVo.setClassInfoList(classInfoVos);
        return BaseResponse.successInstance(branchMenuVo);
    }


}
