package com.lancabbage.gorgeous.controller.project;

import com.lancabbage.gorgeous.bean.dto.ProjectConfigDto;
import com.lancabbage.gorgeous.bean.dto.ProjectDto;
import com.lancabbage.gorgeous.bean.vo.base.BaseResponse;
import com.lancabbage.gorgeous.bean.vo.project.*;
import com.lancabbage.gorgeous.map.ProjectDtoToVo;
import com.lancabbage.gorgeous.service.ProjectService;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * @author: lanyanhua
 * @date: 2020/12/4 10:34 下午
 * @Description: 项目管理
 */
@RestController
@RequestMapping("/project")
public class ProjectController {

    private final ProjectService projectService;
    private final ProjectDtoToVo projectDtoToVo;

    public ProjectController(ProjectService projectService, ProjectDtoToVo projectDtoToVo) {
        this.projectService = projectService;
        this.projectDtoToVo = projectDtoToVo;
    }

    /**
     * 添加项目
     *
     * @return ID
     */
    @PostMapping("/addProject")
    public BaseResponse<Integer> addProject(@RequestBody @Valid ProjectAddVo vo) {
        int id = projectService.addProject(vo);
        return BaseResponse.successInstance(id);
    }

    /**
     * 保存项目
     *
     * @return ID
     */
    @PostMapping("/saveSuperProject")
    public BaseResponse<Integer> saveSuperProject(@RequestBody SuperProjectSaveVo vo) {
        ProjectDto dto = projectDtoToVo.saveSuperProject(vo);
        int id = projectService.saveSuperProject(dto);
        return BaseResponse.successInstance(id);
    }

    /**
     * 保存项目
     *
     * @return ID
     */
    @PostMapping("/saveProject")
    public BaseResponse<String> saveProject(@RequestBody ProjectSaveVo p) {
        projectService.saveProject(p);
        return BaseResponse.successInstance("成功");
    }

    /**
     * 删除项目
     *
     * @param id ID
     */
    @DeleteMapping("/deleteById")
    public BaseResponse<String> deleteById(@RequestParam Integer id) {
        projectService.deleteById(id);
        return BaseResponse.successInstance("成功");
    }

    /**
     * 查询所有项目信息
     *
     * @return 项目信息
     */
    @GetMapping("/listProjectByQuery")
    public BaseResponse<List<ProjectVo>> listProjectByQuery(ProjectQuery query) {
        List<ProjectDto> dtoList = projectService.listProjectByQuery(query);
        List<ProjectVo> projectVos = projectDtoToVo.listProjectDtoToVo(dtoList);
        return BaseResponse.successInstance(projectVos);
    }

    /**
     * 查询选择项目
     *
     * @return 项目信息
     */
    @GetMapping("/listProjectTree")
    public BaseResponse<List<ProjectTreeVo>> listProjectTree() {
        List<ProjectDto> dtoList = projectService.listProjectTree();
        List<ProjectTreeVo> projectVos = projectDtoToVo.listProjectTreeDtoToVo(dtoList);
        return BaseResponse.successInstance(projectVos);
    }


    /**
     * 查询项目配置信息
     *
     * @param id ID
     * @return 端口、上下文路径
     */
    @GetMapping("/listProjectConfigById")
    public BaseResponse<List<ProjectConfigDto>> listProjectConfigById(@RequestParam Integer id) {
        List<ProjectConfigDto> pcs = projectService.listProjectConfigById(id);
        return BaseResponse.successInstance(pcs);
    }

    /**
     * 获取super项目信息
     *
     * @param id ID
     * @return 项目 菜单
     */
    @GetMapping("/getProjectSuperById")
    public BaseResponse<ProjectTreeVo> getProjectSuperById(@RequestParam Integer id) {
        ProjectDto dtoList = projectService.getProjectSuperById(id);
        ProjectTreeVo projectVos = projectDtoToVo.getProjectDtoToVo(dtoList);
        return BaseResponse.successInstance(projectVos);
    }
}
