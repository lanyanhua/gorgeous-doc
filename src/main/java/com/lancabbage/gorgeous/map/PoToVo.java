package com.lancabbage.gorgeous.map;

import com.lancabbage.gorgeous.bean.po.ProjectBranch;
import com.lancabbage.gorgeous.bean.vo.project.ProjectBranchVo;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * @author: lanyanhua
 * @date: 2021/9/2 11:34 上午
 * @Description:
 */
@Mapper
public interface PoToVo {
    PoToVo INSTANCE = Mappers.getMapper(PoToVo.class);

    List<ProjectBranchVo> listProjectBranchVo(List<ProjectBranch> list);
}
