package com.lancabbage.gorgeous.map;

import com.lancabbage.gorgeous.bean.dto.ProjectBranchDto;
import com.lancabbage.gorgeous.bean.dto.ProjectDto;
import com.lancabbage.gorgeous.bean.po.Project;
import com.lancabbage.gorgeous.bean.po.ProjectBranch;
import com.lancabbage.gorgeous.bean.vo.project.*;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * @author: lanyanhua
 * @date: 2020/12/5 3:48 下午
 * @Description:
 */
@Mapper(componentModel = "spring")
public interface ProjectDtoToVo {
    ProjectDtoToVo INSTANCE = Mappers.getMapper(ProjectDtoToVo.class);

    ProjectBranchVo getProjectBranchToVo(ProjectBranch collect);

    Project projectAddVoToPo(ProjectAddVo vo);

    List<ProjectDto> listProjectToDto(List<Project> projects);

    List<ProjectVo> listProjectDtoToVo(List<ProjectDto> dtoList);

    ProjectVo projectDtoToVo(ProjectDto dtoList);


    List<ProjectTreeVo> listProjectTreeDtoToVo(List<ProjectDto> dtoList);

    ProjectDto saveSuperProject(SuperProjectSaveVo vo);


    ProjectDto projectToDto(Project project);

    void copyProject(ProjectSaveVo project, @MappingTarget Project p);


    ProjectTreeVo getProjectDtoToVo(ProjectDto dtoList);

    List<ProjectBranchDto> listProjectBranchToDto(List<ProjectBranch> collect);
}
