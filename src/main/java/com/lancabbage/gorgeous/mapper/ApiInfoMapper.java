package com.lancabbage.gorgeous.mapper;

import com.lancabbage.gorgeous.bean.dto.ApiInfoDto;
import com.lancabbage.gorgeous.bean.po.ApiInfo;
import com.lancabbage.gorgeous.mapper.base.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface ApiInfoMapper extends BaseMapper<ApiInfo> {

    /**
     * 根据分支菜单查询接口
     *
     * @param projectId 项目ID
     */
    List<ApiInfo> listApiByBranchMenu(@Param("projectId") Integer projectId);

    /**
     * 查询被使用的API
     *
     * @param sourceId 分支ID
     * @param targetId 分支ID
     */
    List<ApiInfoDto> listReferencedApiByBranchId(@Param("sourceId") Integer sourceId, @Param("targetId") Integer targetId);

    void updateBranchId(@Param("idList") List<Integer> idList);
}