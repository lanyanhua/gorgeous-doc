package com.lancabbage.gorgeous.mapper;

import com.lancabbage.gorgeous.bean.dto.ClassInfoDto;
import com.lancabbage.gorgeous.bean.po.ClassInfo;
import com.lancabbage.gorgeous.mapper.base.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface ClassInfoMapper extends BaseMapper<ClassInfo> {

    /**
     * 查询项目分支下所有类
     *
     * @param projectIds 项目ID
     * @param branchIds  分支ID
     */
    List<ClassInfoDto> listClassByProjectIds(@Param("projectIds") List<Integer> projectIds, @Param("branchIds") List<Integer> branchIds);

    List<ClassInfoDto> listClassByApiIds(@Param("ids") List<Integer> ids);
}