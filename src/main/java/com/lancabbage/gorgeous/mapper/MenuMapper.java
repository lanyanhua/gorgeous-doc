package com.lancabbage.gorgeous.mapper;

import com.lancabbage.gorgeous.bean.dto.ApiInfoDto;
import com.lancabbage.gorgeous.bean.dto.MenuCriteria;
import com.lancabbage.gorgeous.bean.dto.MenuDto;
import com.lancabbage.gorgeous.bean.po.Menu;
import com.lancabbage.gorgeous.mapper.base.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface MenuMapper extends BaseMapper<Menu> {


    /**
     * 查询菜单API信息
     */
    List<MenuDto> listMenuApiByCriteria(MenuCriteria criteria);

    void updateMenuApi(ApiInfoDto api);
}