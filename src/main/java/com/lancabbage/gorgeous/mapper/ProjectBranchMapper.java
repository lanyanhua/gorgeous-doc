package com.lancabbage.gorgeous.mapper;

import com.lancabbage.gorgeous.bean.dto.ProjectBranchDto;
import com.lancabbage.gorgeous.bean.po.ProjectBranch;
import com.lancabbage.gorgeous.mapper.base.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface ProjectBranchMapper extends BaseMapper<ProjectBranch> {
    /**
     * 分支信息 菜单 api
     *
     * @param id 项目ID
     */
    List<ProjectBranchDto> listBranchMenuById(List<Integer> id);

    /**
     * 项目分支数量
     *
     * @param id 分支ID
     */
    int countBranch(@Param("id") Integer id);

    Integer isReferenced(@Param("id") Integer id);

    ProjectBranchDto getProjectByUrlAndBranchName(@Param("urls") List<String> urls, @Param("ref") String ref);
}