package com.lancabbage.gorgeous.mapper;

import com.lancabbage.gorgeous.bean.dto.ProjectBranchDto;
import com.lancabbage.gorgeous.bean.po.Project;
import com.lancabbage.gorgeous.bean.vo.project.ProjectQuery;
import com.lancabbage.gorgeous.mapper.base.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface ProjectMapper extends BaseMapper<Project> {

    /**
     * 查询所有项目
     */
    List<Project> listProjectByQuery(ProjectQuery query);


}