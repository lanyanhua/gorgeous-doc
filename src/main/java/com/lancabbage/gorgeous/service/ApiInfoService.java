package com.lancabbage.gorgeous.service;

import com.lancabbage.gorgeous.bean.dto.ApiInfoDto;
import com.lancabbage.gorgeous.bean.po.ApiInfo;
import com.lancabbage.gorgeous.bean.vo.menu.MenuQuery;

import java.util.List;

/**
 * API service
 *
 * @author: lanyanhua
 * @date: 2020/12/6 4:07 下午
 * @Description:
 */
public interface ApiInfoService {

    /**
     * 保存API信息
     *
     * @param apiInfos  API info
     * @param projectId 项目ID
     * @param branchId  分支ID
     */
    void addApiList(List<ApiInfoDto> apiInfos, Integer projectId, Integer branchId);

    /**
     * 保存API信息
     *
     * @param apiInfos  API info
     * @param projectId 项目ID
     * @param branchId  分支ID
     */
    void saveApiList(List<ApiInfoDto> apiInfos, Integer projectId, Integer branchId);

    /**
     * 查询API
     *
     * @param query 分支ID
     * @return API
     */
    List<ApiInfoDto> listApiByQuery(MenuQuery query);

    /**
     * 查询API
     *
     * @param branchId 分支ID
     * @return API
     */
    List<ApiInfo> listApiInfoByBranchId(Integer branchId);


    /**
     * 删除API
     *
     * @param id 分支ID
     */
    void deleteByBranchId(List<Integer> id);

    /**
     * 删除API
     *
     * @param projectId 分支ID
     */
    void deleteByProjectId(List<Integer> projectId);

    /**
     * 查询被使用的API
     *
     * @param sourceId 源分支ID
     * @param targetId 目标分支ID
     */
    List<ApiInfoDto> listReferencedApiByBranchId(Integer sourceId, Integer targetId);

    /**
     * 将API分支ID滞空
     *
     * @param idList API ID
     */
    void updateBranchId(List<Integer> idList);
}
