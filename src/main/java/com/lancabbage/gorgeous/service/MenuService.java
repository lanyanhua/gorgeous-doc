package com.lancabbage.gorgeous.service;

import com.lancabbage.gorgeous.bean.dto.ApiInfoDto;
import com.lancabbage.gorgeous.bean.dto.MenuDto;
import com.lancabbage.gorgeous.bean.dto.ProjectBranchDto;
import com.lancabbage.gorgeous.bean.po.Menu;
import com.lancabbage.gorgeous.bean.po.ProjectBranch;

import java.util.List;
import java.util.Map;

/**
 * 菜单service
 *
 * @author: lanyanhua
 * @date: 2020/12/6 12:36 上午
 * @Description:
 */
public interface MenuService {
    /**
     * 新增菜单
     *
     * @param menuDtoList 菜单
     * @param branch      分支
     */
    void addMenuList(Map<String, List<MenuDto>> menuDtoList, ProjectBranch branch);

    /**
     * 保存菜单信息
     *
     * @param menuDtoList 菜单
     * @param b           分支
     */
    void saveMenuList(Map<String, List<MenuDto>> menuDtoList, ProjectBranch b);

    /**
     * 查询菜单
     *
     * @param branchId 分支ID
     * @return 菜单列表 API信息
     */
    List<MenuDto> listMenuByBranchId(Integer branchId);

    /**
     * 查询菜单API信息
     *
     * @param id 分支ID
     */
    List<MenuDto> listMenuApiByBranchId(List<Integer> id);

    /**
     * 查询API关联的菜单
     *
     * @param aIds API ID
     */
    List<Menu> listApiMenuByApiId(List<Integer> aIds);


    /**
     * 删除菜单
     *
     * @param id 分支ID
     */
    void deleteByBranchId(List<Integer> id);

    /**
     * 保存分支菜单
     */
    void saveMenuList(ProjectBranchDto branchDto);

    /**
     * 修改菜单API
     *
     * @param apiList 源API，新API
     */
    void updateMenuApi(List<ApiInfoDto> apiList);

    /**
     * 删除API相关菜单
     *
     * @param idList API ID
     */
    void deleteByApiId(List<Integer> idList);
}
