package com.lancabbage.gorgeous.service;

import com.lancabbage.gorgeous.bean.dto.ApiInfoDto;
import com.lancabbage.gorgeous.bean.dto.ProjectBranchAddDto;
import com.lancabbage.gorgeous.bean.dto.ProjectBranchDto;
import com.lancabbage.gorgeous.bean.po.ProjectBranch;

import java.util.List;

/**
 * 项目分支service
 *
 * @author: lanyanhua
 * @date: 2020/12/5 5:31 下午
 * @Description:
 */
public interface ProjectBranchService {

    /**
     * 根据项目ID查询分支
     */
    List<ProjectBranch> listProjectBranchById(List<Integer> id);

    /**
     * 添加项目分支
     *
     * @param branchAddDto 分支名称，项目信息
     * @return ID
     */
    int addProjectBranch(ProjectBranchAddDto branchAddDto);

    /**
     * 拉git代码
     */
    void pullProjectBranch(ProjectBranchDto branch);

    /**
     * 根据ID删除分支
     */
    void deleteBranchById(Integer id);

    /**
     * 删除分支
     *
     * @param id 项目ID
     */
    void deleteByProjectId(Integer id);

    /**
     * 查询分支菜单
     *
     * @param id 项目ID
     */
    List<ProjectBranchDto> listBranchMenuById(List<Integer> id);

    /**
     * 保存分支菜单
     */
    void saveBranchMenu(ProjectBranchDto branchDto);

    /**
     * 项目分支数量
     *
     * @param id 分支ID
     */
    int countBranch(Integer id);

    /**
     * 当前分支API是否被引用
     *
     * @param id 分支ID
     */
    boolean isReferenced(Integer id);


    /**
     * 项目分支列表
     *
     * @param projectId 项目ID
     * @return 分支列表
     */
    List<ProjectBranch> listBranchByProjectId(Integer projectId);

    /**
     * 将sourceId分支被引用的接口替换为targetId分支接口
     *
     * @param apiList 需要转换的API
     * @param type    转换类型
     */
    void replaceBranch(List<ApiInfoDto> apiList, Integer type);

    /**
     * url 分支名获取项目信息
     * @param urls url
     * @param ref 分支
     */
    ProjectBranchDto getProjectByUrlAndBranchName(List<String> urls, String ref);
}
