package com.lancabbage.gorgeous.service;

import com.lancabbage.gorgeous.bean.dto.ProjectBranchDto;
import com.lancabbage.gorgeous.bean.dto.ProjectConfigDto;
import com.lancabbage.gorgeous.bean.dto.ProjectDto;
import com.lancabbage.gorgeous.bean.po.Project;
import com.lancabbage.gorgeous.bean.vo.project.ProjectAddVo;
import com.lancabbage.gorgeous.bean.vo.project.ProjectQuery;
import com.lancabbage.gorgeous.bean.vo.project.ProjectSaveVo;

import java.util.List;

/**
 * 项目信息service
 *
 * @author: lanyanhua
 * @date: 2020/12/4 10:34 下午
 * @Description:
 */
public interface ProjectService {

    /**
     * 查询所有项目信息
     *
     * @return 项目信息
     */
    List<ProjectDto> listProjectByQuery(ProjectQuery query);

    /**
     * 添加项目
     *
     * @param vo 名称，git地址
     * @return id
     */
    int addProject(ProjectAddVo vo);

    /**
     * 查询项目信息
     *
     * @param projectId 项目ID
     */
    Project getProjectById(Integer projectId);

    /**
     * 保存项目
     */
    void saveProject(ProjectSaveVo vo);

    /**
     * 删除项目
     *
     * @param id ID
     */
    void deleteById(Integer id);

    /**
     * 当前项目数量
     */
    long count();

    /**
     * 查询项目模块名称
     *
     * @param id ID
     * @return 端口、上下文路径
     */
    List<ProjectConfigDto> listProjectConfigById(Integer id);

    /**
     * 查询选择项目
     */
    List<ProjectDto> listProjectTree();

    /**
     * 保存项目
     */
    int saveSuperProject(ProjectDto dto);


    /**
     * 获取super项目信息
     *
     * @param id ID
     * @return 项目 菜单
     */
    ProjectDto getProjectSuperById(Integer id);

}
