package com.lancabbage.gorgeous.service.impl;

import com.lancabbage.gorgeous.bean.dto.ProjectBranchAddDto;
import com.lancabbage.gorgeous.bean.dto.ProjectBranchDto;
import com.lancabbage.gorgeous.bean.dto.ProjectConfigDto;
import com.lancabbage.gorgeous.bean.dto.ProjectDto;
import com.lancabbage.gorgeous.bean.po.Project;
import com.lancabbage.gorgeous.bean.po.ProjectBranch;
import com.lancabbage.gorgeous.bean.po.ProjectConfig;
import com.lancabbage.gorgeous.bean.vo.project.ProjectAddVo;
import com.lancabbage.gorgeous.bean.vo.project.ProjectQuery;
import com.lancabbage.gorgeous.bean.vo.project.ProjectSaveVo;
import com.lancabbage.gorgeous.map.ProjectDtoToVo;
import com.lancabbage.gorgeous.mapper.ProjectConfigMapper;
import com.lancabbage.gorgeous.mapper.ProjectMapper;
import com.lancabbage.gorgeous.service.ProjectBranchService;
import com.lancabbage.gorgeous.service.ProjectService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.springframework.util.CollectionUtils;
import tk.mybatis.mapper.entity.Example;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author: lanyanhua
 * @date: 2020/12/5 3:52 下午
 * @Description:
 */
@Service
public class ProjectServiceImpl implements ProjectService {

    private final ProjectMapper projectMapper;
    private final ProjectConfigMapper projectConfigMapper;
    private final ProjectBranchService projectBranchService;

    public ProjectServiceImpl(ProjectMapper projectMapper, ProjectConfigMapper projectConfigMapper
            , ProjectBranchService projectBranchService) {
        this.projectMapper = projectMapper;
        this.projectConfigMapper = projectConfigMapper;
        this.projectBranchService = projectBranchService;
    }


    @Override
    public List<ProjectDto> listProjectByQuery(ProjectQuery query) {
        List<Project> projects = projectMapper.listProjectByQuery(query);
        List<ProjectDto> dtoList = new ArrayList<>();
        if (projects.isEmpty()) {
            return dtoList;
        }
        dtoList = ProjectDtoToVo.INSTANCE.listProjectToDto(projects);
        //查询分支
        List<ProjectBranch> projectBranches = projectBranchService.listProjectBranchById(projects.stream()
                .map(Project::getId)
                .collect(Collectors.toList()));
        //查询项目配置
        List<ProjectConfigDto> pcs = projectConfigMapper.listByProjectIds(projects.stream()
                .filter(i -> i.getRemotePath() != null)
                .map(Project::getId)
                .collect(Collectors.toList()));
        for (ProjectDto projectDto : dtoList) {
            List<ProjectBranch> collect = projectBranches.stream()
                    .filter(i -> i.getProjectId().equals(projectDto.getId()))
                    .collect(Collectors.toList());
            projectDto.setBranchList(ProjectDtoToVo.INSTANCE.listProjectBranchToDto(collect));
            //配置信息
            if (projectDto.getRemotePath() != null) {
                List<ProjectConfigDto> pc = pcs.stream()
                        .filter(i -> i.getProjectId().equals(projectDto.getId()))
                        .collect(Collectors.toList());
                projectDto.setProjectConfigs(pc);
            }
        }
        return dtoList;
    }

    @Transactional
    @Override
    public int addProject(ProjectAddVo vo) {
        assertName(vo.getName());
        Project project = ProjectDtoToVo.INSTANCE.projectAddVoToPo(vo);
        project.setCreateTime(new Date());
        projectMapper.insert(project);
        //项目默认配置
        ProjectConfig pc = new ProjectConfig();
        pc.setProjectId(project.getId());
        pc.setMenuName(vo.getBranchName());
        pc.setName("默认");
        pc.setCreateTime(new Date());
        projectConfigMapper.insert(pc);
        //分支
        ProjectBranchAddDto branchAddDto = new ProjectBranchAddDto();
        branchAddDto.setName(vo.getBranchName());
        branchAddDto.setProject(project);
        // git信息 项目名
        projectBranchService.addProjectBranch(branchAddDto);
        return project.getId();
    }

    @Override
    public Project getProjectById(Integer projectId) {
        return projectMapper.selectByPrimaryKey(projectId);
    }

    @Transactional
    @Override
    public void saveProject(ProjectSaveVo p) {
        Project project = projectMapper.selectByPrimaryKey(p.getId());
        Assert.notNull(project, "项目不存在");
        ProjectDtoToVo.INSTANCE.copyProject(p, project);
        projectMapper.updateByPrimaryKey(project);
        //保存配置信息
        if (CollectionUtils.isEmpty(p.getProjectConfigs())) {
            return;
        }
        //修改
        projectConfigMapper.updateByPrimaryKeySelectiveList(p.getProjectConfigs().stream()
                .filter(i -> i.getId() != null)
                .collect(Collectors.toList()));
        //新增
        List<ProjectConfig> collect = p.getProjectConfigs().stream()
                .filter(i -> i.getId() == null)
                .peek(i -> {
                    i.setCreateTime(new Date());
                    i.setProjectId(p.getId());
                })
                .collect(Collectors.toList());
        if (collect.isEmpty()) {
            return;
        }
        projectConfigMapper.insertList(collect);
    }

    @Transactional
    @Override
    public void deleteById(Integer id) {
        projectMapper.deleteByPrimaryKey(id);
        Example example = new Example(ProjectConfig.class);
        example.createCriteria().andEqualTo("projectId", id);
        projectConfigMapper.deleteByExample(example);
        projectBranchService.deleteByProjectId(id);
    }

    @Override
    public long count() {
        return projectMapper.selectCount(null);
    }

    @Override
    public List<ProjectConfigDto> listProjectConfigById(Integer id) {
        Project project = projectMapper.selectByPrimaryKey(id);
        Assert.notNull(project, "项目ID不存在");
//        if (project.getRemotePath() != null) {
//            return projectConfigMapper.listByProjectIds(Collections.singletonList(id));
//        }
        return projectConfigMapper.listByApplicationId(id);
    }

    @Override
    public List<ProjectDto> listProjectTree() {
        //只查询git地址不为null的项目
        Example example = new Example(Project.class);
        example.createCriteria().andIsNotNull("remotePath");
        List<Project> projects = projectMapper.selectByExample(example);
        List<ProjectDto> dtoList = new ArrayList<>();
        if (projects.isEmpty()) {
            return dtoList;
        }
        dtoList = ProjectDtoToVo.INSTANCE.listProjectToDto(projects);
        //查询分支&菜单
        List<ProjectBranchDto> branchList = projectBranchService.listBranchMenuById(projects.stream()
                .map(Project::getId).collect(Collectors.toList()));
        for (ProjectDto projectVo : dtoList) {
            projectVo.setBranchList(branchList.stream()
                    .filter(i -> i.getProjectId().equals(projectVo.getId()))
                    .collect(Collectors.toList()));
        }
        return dtoList;
    }

    @Transactional
    @Override
    public int saveSuperProject(ProjectDto dto) {
        //保存项目信息
        if (dto.getId() == null) {
            assertName(dto.getName());
            dto.setCreateTime(new Date());
            projectMapper.insertSelective(dto);
        } else {
            projectMapper.updateByPrimaryKeySelective(dto);
        }
        //保存分支
        ProjectBranchDto branchDto = new ProjectBranchDto();
        branchDto.setName("master");
        branchDto.setProjectId(dto.getId());
        branchDto.setMenuList(dto.getMenuList());
        projectBranchService.saveBranchMenu(branchDto);
        return dto.getId();
    }

    @Override
    public ProjectDto getProjectSuperById(Integer id) {
        Project project = projectMapper.selectByPrimaryKey(id);
        Assert.notNull(project, "项目不存在");
        ProjectDto dto = ProjectDtoToVo.INSTANCE.projectToDto(project);
        //查询分支&菜单
        List<ProjectBranchDto> branchList = projectBranchService.listBranchMenuById(Collections.singletonList(
                project.getId()));
        dto.setBranchList(branchList);
        return dto;
    }


    private void assertName(String name) {
        Example example = new Example(Project.class);
        example.createCriteria().andEqualTo("name", name);
        int count = projectMapper.selectCountByExample(example);
        Assert.isTrue(count == 0, "项目名已存在，请选择新增分支");
    }
}
