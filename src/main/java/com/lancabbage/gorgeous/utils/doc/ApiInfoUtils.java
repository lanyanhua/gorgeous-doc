package com.lancabbage.gorgeous.utils.doc;

import com.lancabbage.gorgeous.bean.dto.ApiInfoDto;
import com.lancabbage.gorgeous.bean.dto.ApiParamDto;
import com.lancabbage.gorgeous.bean.dto.ClassInfoDto;
import com.lancabbage.gorgeous.bean.dto.MenuDto;
import com.lancabbage.gorgeous.enums.ParamModeEnum;
import com.lancabbage.gorgeous.enums.ParamTypeEnum;
import com.thoughtworks.qdox.JavaProjectBuilder;
import com.thoughtworks.qdox.model.*;
import tk.mybatis.mapper.util.StringUtil;

import java.io.File;
import java.util.*;

import static com.lancabbage.gorgeous.utils.doc.NotesConfigUtils.s;

/**
 * @author: lanyanhua
 * @date: 2020/12/4 8:38 下午
 * @Description:
 */
public class ApiInfoUtils {

    private final AnnotationUtils annotationUtils;
    private final List<ApiInfoDto> apiAll;
    private ClassInfoUtils classInfoUtils;

    public ApiInfoUtils() {
        this.annotationUtils = new AnnotationUtils();
        apiAll = new ArrayList<>();
    }

    public Collection<ClassInfoDto> getClassInfoList() {
        return classInfoUtils.getClassMap().values();
    }

    public List<ApiInfoDto> getApiAll() {
        return apiAll;
    }

    /**
     * 解析类
     *
     * @param sources 文件路径
     * @return 菜单API
     */
    public Map<String, List<MenuDto>> parsingClass(List<String> sources) {
        // 创建 java 项目 builder 对象
        JavaProjectBuilder javaProjectBuilder = new JavaProjectBuilder();
        this.classInfoUtils = new ClassInfoUtils(annotationUtils, javaProjectBuilder);
        // 添加 java 源文件
        for (String source : sources) {
            javaProjectBuilder.addSourceTree(new File(source));
        }
        // 获得解析后的类
        Collection<JavaClass> classes = javaProjectBuilder.getClasses();
        Map<String, List<MenuDto>> menuMap = new HashMap<>();
        //组装对象
        for (JavaClass classDoc : classes) {
            List<JavaAnnotation> annotations = classDoc.getAnnotations();
            AnnotationRes controller = annotationUtils.isController(annotations);
            //注解 判断是否是controller RestController 还有路径RequestMapping({"/jobGroup"})
            if (!controller.isController || classDoc.isAnnotation()) {
                continue;
            }
            //组装菜单
            MenuDto m = new MenuDto();
            //类名
            m.setClassName(classDoc.getName());
            //tag描述
            annotationUtils.setParmTag(NotesConfigUtils.getClassTag());
            String tagDesc = annotationUtils.getTagDesc(classDoc.getTags());
            annotationUtils.setParmTag(NotesConfigUtils.getClassAnnotation());
            String annotationDesc = annotationUtils.getAnnotationDesc(annotations);
            //获取注释
            m.setMenuName(s(annotationDesc, tagDesc, classDoc.getComment(), classDoc.getName()));
            //api
            m.setApiInfos(parsingMethod(controller, classDoc));

            String path = classDoc.getSource().getURL().getPath();
            //win linux获取的路径不同
            int src = path.indexOf("/src/") & path.indexOf("\\src\\");
            int n = path.lastIndexOf("/", src - 1) & path.lastIndexOf("\\", src - 1);
            String key = path.substring(n + 1, src);
            List<MenuDto> menuDtoList = menuMap.computeIfAbsent(key, k -> new ArrayList<>());
            menuDtoList.add(m);
        }
        return menuMap;
    }

    /**
     * 解析方法
     *
     * @param cc controller类信息
     * @param c  类
     * @return API
     */
    private List<ApiInfoDto> parsingMethod(AnnotationRes cc, JavaClass c) {
        List<ApiInfoDto> apiInfos = new ArrayList<>();
        //先读取父类的
        List<JavaClass> interfaces = c.getInterfaces();

        if (interfaces != null && interfaces.size() > 0) {
            for (JavaClass anInterface : interfaces) {
                apiInfos.addAll(parsingMethod(cc, anInterface));
            }
        }
        //
        List<JavaMethod> methods = c.getMethods();
        for (JavaMethod method : methods) {
            //解析注解
            List<JavaAnnotation> annotations = method.getAnnotations();
            AnnotationRes isApi = annotationUtils.isApi(annotations);
            //不是API下一个
            if (isApi.apiType == -1) {
                continue;
            }
            ApiInfoDto apiInfo = new ApiInfoDto();
            //接口描述 方法名
            annotationUtils.setParmTag(NotesConfigUtils.getMethodTag());
            String tagDesc = annotationUtils.getTagDesc(method.getTags());
            annotationUtils.setParmTag(NotesConfigUtils.getMethodAnnotation());
            String annotationDesc = annotationUtils.getAnnotationDesc(annotations);
            apiInfo.setName(s(annotationDesc, tagDesc, method.getComment(), method.getName()));
            apiInfo.setMethod(method.getName());
            //接口类型 接口访问路径
            apiInfo.setType(isApi.apiType);
            apiInfo.setPath(s(cc.path + isApi.path, "/"));
            //参数信息
            List<ApiParamDto> apiParams = new ArrayList<>();
            apiInfo.setApiParamList(apiParams);
            //返回参数
            apiParams.add(getOutParam(method));
            //入参
            apiParams.addAll(getInputParam(method));
            apiInfos.add(apiInfo);
            apiAll.add(apiInfo);
        }

        return apiInfos;
    }

    /**
     * 出参
     *
     * @param method method
     * @return 出参
     */
    private ApiParamDto getOutParam(JavaMethod method) {
        //返回值
        ApiParamDto retParam = new ApiParamDto();
        //todo getReturnType
        JavaClass returnType = (JavaClass) method.getReturnType();
        //返回值类名称
        String resClassName = returnType.getName();
        retParam.setParamName(resClassName);
        retParam.setType(ParamTypeEnum.OUT_PARAM.getCode());
        //tag描述
        annotationUtils.setParmTag(NotesConfigUtils.getMethodReturnTag());
        retParam.setParamDescribe(annotationUtils.getTagDesc(method.getTags()));
        //赋值数据类型
        setDataType(retParam, returnType);
        return retParam;
    }

    /**
     * 入参
     *
     * @param method method
     * @return 入参
     */
    private List<ApiParamDto> getInputParam(JavaMethod method) {
        //参数注释信息
        annotationUtils.setParmTag(NotesConfigUtils.getMethodParamTag());
        Map<String, String> paramMap = annotationUtils.getParamMap(method.getTags());
        //参数
        List<JavaParameter> parameters = method.getParameters();
        List<ApiParamDto> apiParams = new ArrayList<>();
        if (parameters == null || parameters.size() == 0) {
            return apiParams;
        }
        for (JavaParameter parameter : parameters) {
            String name = parameter.getName();
            ApiParamDto apiParamDto = new ApiParamDto();
            apiParams.add(apiParamDto);
            apiParamDto.setType(ParamTypeEnum.INPUT_PARAM.getCode());
            //参数名称，描述
            apiParamDto.setParamName(name);
            apiParamDto.setParamDescribe(paramMap.get(name));
            //参数传输方式 1：form-data 2：post json格式 3：path {id}
            apiParamDto.setParamMode(ParamModeEnum.FORM_DATA.getCode());
            for (JavaAnnotation annotation : parameter.getAnnotations()) {
                String name1 = annotation.getType().getName();
                int paramMode = annotationUtils.getParamMode(name1);
                if (paramMode != -1) {
                    apiParamDto.setParamMode(paramMode);
                    String paramValue = annotationUtils.getParamValue(annotation, Collections.singletonList("value"));
                    if (!StringUtil.isEmpty(paramValue)) {
                        apiParamDto.setParamName(paramValue);
                    }
                }
            }
            //赋值数据类型
            setDataType(apiParamDto, parameter.getType());
        }
        return apiParams;
    }

    private void setDataType(ApiParamDto paramDto, JavaType type) {
        JavaClass type1 = (JavaClass) type;
        String typeName = type1.getName();
        ClassInfoDto classInfo = classInfoUtils.getClassInfo(type);
        if (classInfo.getBaseType() != null) {
            paramDto.setDataType(typeName);
        } else {
            paramDto.setDataType(classInfo.getClassName());
            paramDto.setClassInfo(classInfo);
        }
    }


}
