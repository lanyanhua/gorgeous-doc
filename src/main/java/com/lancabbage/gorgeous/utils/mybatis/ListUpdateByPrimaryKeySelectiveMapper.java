package com.lancabbage.gorgeous.utils.mybatis;

import org.apache.ibatis.annotations.UpdateProvider;
import tk.mybatis.mapper.annotation.RegisterMapper;

import java.util.List;

/**
 * 通用Mapper接口,更新
 *
 * @param <T> 不能为空
 * @author lanyanhua
 */
@RegisterMapper
public interface ListUpdateByPrimaryKeySelectiveMapper<T> {

    /**
     * 根据主键更新属性不为null的值
     *
     * @param record
     * @return
     */
    @UpdateProvider(type = ListUpdateProvider.class, method = "dynamicSQL")
    int updateByPrimaryKeySelectiveList(List<T> record);

}