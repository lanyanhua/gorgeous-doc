package com.lancabbage.gorgeous.utils.mybatis;

import org.apache.ibatis.mapping.MappedStatement;
import tk.mybatis.mapper.entity.EntityColumn;
import tk.mybatis.mapper.mapperhelper.EntityHelper;
import tk.mybatis.mapper.mapperhelper.MapperHelper;
import tk.mybatis.mapper.mapperhelper.MapperTemplate;
import tk.mybatis.mapper.mapperhelper.SqlHelper;

import java.util.Set;

import static tk.mybatis.mapper.mapperhelper.SqlHelper.whereVersion;

/**
 * ListUpdateProvider实现类，批量修改方法实现类
 *
 * @author lanyanhua
 */
public class ListUpdateProvider extends MapperTemplate {

    public ListUpdateProvider(Class<?> mapperClass, MapperHelper mapperHelper) {
        super(mapperClass, mapperHelper);
    }

//    /**
//     * 通过主键更新全部字段
//     *
//     * @param ms
//     */
//    public String updateByPrimaryKey(MappedStatement ms) {
//        Class<?> entityClass = getEntityClass(ms);
//        StringBuilder sql = new StringBuilder();
//        sql.append(SqlHelper.updateTable(entityClass, tableName(entityClass)));
//        sql.append(SqlHelper.updateSetColumns(entityClass, null, false, false));
//        sql.append(SqlHelper.wherePKColumns(entityClass, true));
//        return sql.toString();
//    }

    /**
     * 通过主键更新不为null的字段
     *
     * @param ms
     * @return
     */
    public String updateByPrimaryKeySelectiveList(MappedStatement ms) {
        Class<?> entityClass = getEntityClass(ms);
        StringBuilder sql = new StringBuilder();
        sql.append("<foreach collection=\"list\" item=\"record\" >");
        //set
        sql.append(SqlHelper.updateTable(entityClass, tableName(entityClass)));
        sql.append(SqlHelper.updateSetColumns(entityClass, "record", true, isNotEmpty()));
        //where
        sql.append("<where>");
        Set<EntityColumn> columnSet = EntityHelper.getPKColumns(entityClass);
        //当某个列有主键策略时，不需要考虑他的属性是否为空，因为如果为空，一定会根据主键策略给他生成一个值
        for (EntityColumn column : columnSet) {
            sql.append(" AND ").append(column.getColumnEqualsHolder("record"));
        }
        sql.append(whereVersion(entityClass));
        sql.append("</where>");
        sql.append(" ;\n");

        sql.append("</foreach>");
        return sql.toString();
    }
}
