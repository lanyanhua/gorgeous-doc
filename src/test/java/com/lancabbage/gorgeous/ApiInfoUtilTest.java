package com.lancabbage.gorgeous;

import com.alibaba.fastjson.JSON;
import com.lancabbage.gorgeous.bean.dto.MenuDto;
import com.lancabbage.gorgeous.bean.po.NotesConfig;
import com.lancabbage.gorgeous.utils.doc.AnnotationUtils;
import com.lancabbage.gorgeous.utils.doc.ApiInfoUtils;
import com.lancabbage.gorgeous.utils.doc.ClassInfoUtils;
import com.lancabbage.gorgeous.utils.doc.NotesConfigUtils;
import com.thoughtworks.qdox.JavaProjectBuilder;
import com.thoughtworks.qdox.model.JavaClass;
import com.thoughtworks.qdox.model.JavaMethod;
import com.thoughtworks.qdox.model.JavaParameter;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author: lanyanhua
 * @date: 2020/12/5 1:47 下午
 * @Description:
 */
public class ApiInfoUtilTest {
    @BeforeEach
    public void beforeEach() {
        //注释不读库
        List<NotesConfig> notesConfigList = new ArrayList<>();
        notesConfigList.add(NotesConfigUtils.notesConfig("classTag", "@Description:"));
        notesConfigList.add(NotesConfigUtils.notesConfig("methodParamTag", "@param"));
        notesConfigList.add(NotesConfigUtils.notesConfig("methodReturnTag", "@return"));
        notesConfigList.add(NotesConfigUtils.notesConfig("classAnnotation", "@Api(tags)"));
        notesConfigList.add(NotesConfigUtils.notesConfig("methodAnnotation", "@ApiOperation(value)"));
        notesConfigList.add(NotesConfigUtils.notesConfig("fieldAnnotation", "@ApiModelProperty(value)"));
        notesConfigList.add(NotesConfigUtils.notesConfig("arrayType", "List"));
        notesConfigList.add(NotesConfigUtils.notesConfig("arrayType", "Set"));
        notesConfigList.addAll(Stream.of("void", "String", "Object", "byte", "Byte", "short", "Short",
                "int", "Integer", "long", "Long", "double", "Double", "float", "Float", "char", "Char", "boolean", "Boolean",
                "Date", "MultipartFile", "BigDecimal", "URL", "HttpServletResponse", "HttpServletRequest",
                "LinkedHashMap", "HashMap", "Map")
                .map(i -> NotesConfigUtils.notesConfig("baseDataType", i)).collect(Collectors.toList()));
        NotesConfigUtils.notesConfigList = notesConfigList;
    }


    @Test
    public void test() {
//        String basePath = "/Users/lanyanhua/Desktop/gittest/gyl/master";
//        String basePath = "/Users/lanyanhua/Desktop/gittest/qns/2.4.3";
//        String basePath = "/Users/lanyanhua/Desktop/gittest/lan-code-api/master";
//        String basePath = "/Users/lanyanhua/Desktop/gittest/jianan-station/station1.0";
//        String basePath = "/Users/lanyanhua/Desktop/gittest/gorgeous-doc/master/gorgeous-doc";
        String basePath = "/Users/lanyanhua/Documents/workspace/gorgeous-doc";
        String publicPath = "/Users/lanyanhua/Desktop/gittest/public";
//        File file = new File(basePath);
//        File publicFile = new File(publicPath);
//        List<String> javaFile = GitUtils.getJavaFile(file);
//        javaFile.addAll(GitUtils.getJavaFile(publicFile));
        List<String> javaFile = Arrays.asList(publicPath, basePath);
//        javaFile.add("/Users/lanyanhua/Desktop/gittest/public/main/java/com/jaagro/utils/BaseResponse.java");
        ApiInfoUtils classDocUtils = new ApiInfoUtils();
        Map<String, List<MenuDto>> menuDtoList = classDocUtils.parsingClass(javaFile);
        System.out.println(JSON.toJSONString(menuDtoList));
    }

    @Test
    public void test1() throws IOException {
        JavaProjectBuilder javaProjectBuilder = new JavaProjectBuilder();
        ClassInfoUtils classInfoUtils = new ClassInfoUtils(new AnnotationUtils(),javaProjectBuilder);
        javaProjectBuilder.addSourceTree(new File("/Users/lanyanhua/Desktop/gittest/public"));
        javaProjectBuilder.addSourceTree(new File("/Users/lanyanhua/Documents/workspace/gorgeous-doc"));
        JavaClass classByName = javaProjectBuilder.getClassByName("com.lancabbage.gorgeous.controller.TestController");
//        JavaClass classByName = javaProjectBuilder.getClassByName("com.lancabbage.gorgeous.bean.vo.base.BaseResponse");
//((DefaultJavaParameterizedType)classByName.getMethods().get(2).get()).getActualTypeArguments()
//        String simpleTypeName = javaType.getValue();
//        typeName = DocClassUtil.rewriteRequestParam(typeName);
//        gicTypeName = DocClassUtil.rewriteRequestParam(gicTypeName);
//        //if params is collection
//        if (JavaClassValidateUtil.isCollection(typeName)) {
//            apiMethodDoc.setListParam(true);
//        }
//        JavaClass javaClass = configBuilder.getJavaProjectBuilder().getClassByName(typeName);
//        String[] globGicName = DocClassUtil.getSimpleGicName(gicTypeName);
//        String comment = this.paramCommentResolve(paramsComments.get(paramName));
        for (JavaClass aClass : javaProjectBuilder.getClasses()) {
            if (!aClass.getTypeParameters().isEmpty()) {
                System.out.println("d");
            }
            for (JavaMethod method : aClass.getMethods()) {
                if(!method.getTypeParameters().isEmpty()){
                    System.out.println("d");
                }
                if (!method.getReturns().getTypeParameters().isEmpty()) {
                    System.out.println("d");
                }
                for (JavaParameter parameter : method.getParameters()) {

                    if(!parameter.getJavaClass().getTypeParameters().isEmpty()){
                        System.out.println("d");
                    }
                }

            }
        }
        classInfoUtils.getClassInfo(classByName);
    }

}
